package mail_ru.project.test.smtp_utils;


import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;


public class SendMail {
    public static void sendMessage(String from, String to, String subject, String text, String[] attacments,String encoding, String type) throws MessagingException, IOException {

        String smtpHost = "mxs.mail.ru"; //host of smtp mail server

        String localHost = "mail.ru";  //for helo
        String user=from;  //send from
        String password="1";
        String port =  "25";
        Properties props = new Properties();
        props.put("mail.smtp.localhost",localHost);
        props.put("mail.smtp.host", smtpHost);
        props.setProperty("mail.user", user);
        props.setProperty("mail.password", password);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.debug", "true");
        SmtpMessageSender messageSender = new SmtpMessageSender();
                MimeMessage message = messageSender.createMimeMessage(messageSender.createSession(smtpHost,25,user,password,localHost),
                        subject,
                        from,
                        to,
                        Message.RecipientType.TO);
                messageSender.addText(message,
                        text,
                        encoding,
                        type);
                //adds the text file
                if (attacments.length>0) {
                    for (String attach:attacments)
                        messageSender.addAttachment(message,
                        new File(attach));
                }
                message.setSentDate(new Date());
        messageSender.sendMimeMessage(message);

         }
}

