package mail_ru.project.test.pages;


import mail_ru.project.test.smtp_utils.SendMail;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import javax.mail.MessagingException;
import java.io.IOException;


/**
 * Created by areut on 26.02.16.
 */
public class SendMailPage extends Page{
    public SendMailPage(WebDriver webDriver) {
        super(webDriver);
    }
    @FindBy(xpath = "//textarea[@data-original-name='To']")
    private WebElement emailTo;
    @FindBy(name = "Subject")
    private WebElement subject;
    @FindBy(id = "tinymce")
    private WebElement textMail;
    @FindBy(xpath = "//div[@data-shortcut='ctrl+enter|command+enter']/span")
    private WebElement sendButton;
    @FindBy(xpath = "//input[@type='file']")
    private WebElement attach;
    @FindBy(xpath = "//div[@class='is-compose-empty_in']//button[@class='btn btn_stylish btn_main confirm-ok']")
    private WebElement confirmOk;
    private void checkSuccsefullySend()
    {
        try
        {
            shouldBeVisible(By.xpath("//div[@class='message-sent message-sent_IsSocialConnect']"));
        }
        catch (TimeoutException e1){Assert.fail("Message was not sended");};
    }

    private void typeEmailTo(String emailTo) {this.emailTo.sendKeys(emailTo);}
    private void typeSubject(String subject) { this.subject.sendKeys(subject);}
    public void sendMessage(){sendButton.click();}
    public void clickSendButton()
    {
        try{sendButton.click();}catch (StaleElementReferenceException e1){webDriver.findElement(By.xpath("//div[@data-shortcut='ctrl+enter|command+enter']/span")).click();}
    }

    public void sendMessageSMTP(String emailTo,String subject) throws IOException, MessagingException
    {
        String[] attach={};
        SendMail.sendMessage(emailTo, emailTo, subject, "Body text", attach, "utf-8", "plane");
    }
    public void typeEmailToAndSubject(String email,String subject)
    {
        typeEmailTo(email);
        typeSubject(subject);
    }
    public void sendEmptyMessage()
    {
        clickSendButton();
        confirmOk.click();
    }
    public void checkResultOfSending(){checkSuccsefullySend();}



}



