package mail_ru.project.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class HomePage extends Page {

	public HomePage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(id = "mailbox__login")
	private WebElement loginInput;
	@FindBy(id = "mailbox__password")
	private WebElement passwordInput;
	@FindBy(id = "mailbox__auth__button")
	private WebElement logginButton;
	@FindBy(className = "mailbox__register__link")
	private WebElement registationLink;
	@FindBy(className = "mailbox__register__promo__link")
	private WebElement registationLinkWithVideo;
	@FindBy(className = "mailbox__forget-link")
	private WebElement restorePaswordLink;
	@FindBy(className = "mailbox__fodsfdsfdfrget-link")
	private WebElement restorePaswordLink1;

	public void typeLogin(String login)
	{
		loginInput.clear();
		loginInput.sendKeys(login);
	}
	public void typePassword(String password) {passwordInput.sendKeys(password);}
	public void navigateToRegistrationPage() throws InterruptedException {clickRegistrationButton();}
	public void clickLoginButton() {
		logginButton.click();
	}
	public void clickRegistrationButton() {
		registationLink.click();
	}
	public void loginInMail(String login, String password)
	{
		typeLogin(login);
		typePassword(password);
		clickLoginButton();
		checkNoCapchaExist();
	}
	private void checkNoCapchaExist()
	{
		List<WebElement> list = webDriver.findElements(By.xpath("//input[contains(@class,'js-captcha form__field password-recovery__remind__new-code__input')]"));
		if (list.size() > 0)
		{
			Assert.fail("There are capchaon page, it is not expected");
		}
	}
	public void checkErrorDuringLogin()
	{
		List<WebElement> list = webDriver.findElements(By.xpath("//div[contains(@class,'login-page__external_error login-page__external__text_a js-login-page__external__info')]"));
		if (list.size() > 0)
		{
			Assert.fail("There are capcha on page, it is not expected");
		}
	}
	public void checkThatCorrectUserLoginIn(String login)
	{
		String expectedUser = login.concat("@").concat("mail.ru");
		String realUSer = webDriver.findElement(By.id("PH_user-email")).getText();
		if (!realUSer.equals(expectedUser))
		{
			Assert.fail("Not correct user loggin in, Exprecet: " + expectedUser + " but loggin in: " + realUSer);
		}
	}
	public void checkThatLoginIsCompleted()
	{
		shouldBeVisible(By.id("PH_user-email"));
		shouldNotBeVisible(By.id("mailbox__login"));
	}


}
