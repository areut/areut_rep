package mail_ru.project.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

/**
 * Created by areut on 26.02.16.
 */
public class RegistrationPage extends Page {
    public RegistrationPage(WebDriver webDriver) {
        super(webDriver);
    }
    @FindBy(xpath = "//div[contains(@class,'vmi tac mb14 t0 vt qc-firstname-row')]//input")
    private WebElement nameInput;
    @FindBy(xpath = "//div[contains(@class,'vmi tac mb14 t0 vt  qc-lastname-row')]//input")
    private WebElement secondNameInput;
    @FindBy(xpath = "//div[contains(@class,'pRel vmi tac mb14 t0 vt login-field  qc-login-row')]//input")
    private WebElement emailInput;
    @FindBy(xpath = "//div[contains(@class,'vmi tac mb14 t0 vt  qc-pass-row')]//input")
    private WebElement passwordInput;
    @FindBy(xpath = "//div[contains(@class,'vmi tac mb34 t0  qc-passverify-row')]//input")
    private WebElement passwordInputRepeat;
    @FindBy(id = "noPhoneLink")
    private WebElement noPhoneLink;
    @FindBy(xpath = "//*[@class='btn btn_signup js-submit']")
    private WebElement submitButton;
    @FindBy(id = "man1")
    private WebElement maleCheckbox;

    public void typeName(String name){nameInput.sendKeys(name);}
    public void typeSecondName(String value){ secondNameInput.sendKeys(value);}
    public void typeEmail(String value){ emailInput.sendKeys(value);}
    public void typePassword(String value){ passwordInput.sendKeys(value);}
    public void typePAssswordRepeat(String value){ passwordInputRepeat.sendKeys(value);}
    public void clickNoPhoneLink(){noPhoneLink.click();}
    public void clickRegistrationButton(){submitButton.click();}
    private void selectDate(String value)
    {
        Select dropdown = new Select( webDriver.findElement(By.xpath("//select[@class='fll days mt0 mb0 qc-select-day']")));
        dropdown.selectByVisibleText(value);
    }
    private void selectMonth(String value)
    {
        Select dropdown = new Select( webDriver.findElement(By.xpath("//select[@class='fll months mt0 mb0 qc-select-month']")));
        dropdown.selectByIndex(2);
    }
    private void selectYear(String value)
    {
        Select dropdown = new Select( webDriver.findElement(By.xpath("//select[@class='flr years mt0 mb0 qc-select-year']")));
        dropdown.selectByVisibleText(value);
    }
    public void fillEmailAndPasword(String login,String password)
    {
        typeEmail(login);
        typePassword(password);
        typePAssswordRepeat(password);
    }
    public void fillNameAndSecondNAme(String name,String secondName) {
        typeName(name);
        typeSecondName(secondName);
    }
    public void fillBithday(String day,String month,String year)
    {

        selectDate(day);
        selectMonth(month);
        selectYear(year);
    }
    public void finishRegistration() throws InterruptedException {
        clickNoPhoneLink();
        maleCheckbox.click();
        checkErrorsValidationIsExist();
        clickRegistrationButton();
    }

    private void checkErrorsValidationIsNotExist()
    {
        //Use List because if we can get text of these errors
        List<WebElement> list= webDriver.findElements(By.xpath("//div[contains(@class,'sig-error-on')]"));
        if(list.size()>0)
        {
            Assert.fail("There are  validations Errors");
        }
    }
    public void checkErrorsValidationIsExist()
    {
        //Use List because if we can get text of these errors
        List<WebElement> list= webDriver.findElements(By.xpath("//div[contains(@class,'sig-error-on')]"));
        if(list.size()==0)
        {
            Assert.fail("There are no expected validations Errors");
        }
    }


}
