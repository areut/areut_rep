package mail_ru.project.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class Page {
	private static final int ONE_SECOND = 1000;
	private long waitForTimeoutInMilliseconds = 5 * ONE_SECOND;
	protected WebDriver webDriver;

	/*
	 * Constructor injecting the WebDriver interface
	 * 
	 * @param webDriver
	 */
	public Page(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}

	public String getTitle() {
		return webDriver.getTitle();
	}
	public void logOut() {
		shouldBeVisible(By.id("PH_logoutLink"));
		webDriver.findElement(By.id("PH_logoutLink")).click();
		while (webDriver.findElement(By.id("PH_logoutLink")).isDisplayed())
		{webDriver.findElement(By.id("PH_logoutLink")).click();}

	}
	public void shouldBeVisible(final By byCriteria)
	{
		waitOnPage().until(ExpectedConditions.visibilityOfElementLocated(byCriteria));
	}
	private WebDriverWait waitOnPage() {
		return new WebDriverWait(webDriver, waitForTimeoutInSeconds());
	}
	private long waitForTimeoutInSeconds()
	{
		return (waitForTimeoutInMilliseconds < 1000) ? 1 : (waitForTimeoutInMilliseconds/1000);
	}
	public void shouldNotBeVisible(final By byCriteria)
	{
		List<WebElement> matchingElements = webDriver.findElements(byCriteria);
		if (!matchingElements.isEmpty()) {
			waitOnPage().until(ExpectedConditions.invisibilityOfElementLocated(byCriteria));
		}
	}
	public void waitElemetBy(By criteria)
	{
		WebDriverWait wait = new WebDriverWait(webDriver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(criteria));
	}
	public void sleep(int seconds)
	{
		new WebDriverWait(webDriver,seconds);
	}
}
