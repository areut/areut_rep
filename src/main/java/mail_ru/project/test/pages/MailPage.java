package mail_ru.project.test.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.List;

/**
 * Created by areut on 26.02.16.
 */
public class MailPage extends Page {
    public MailPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//a[@data-shortcut-title='N']")
    private WebElement writeMailButton;
    @FindBy(xpath = "//a[@href='/addressbook']")
    private WebElement contacts;
    @FindBy(xpath = "//a[@href='/filesearch']")
    private WebElement files;
    @FindBy(xpath = "//a[@href='/settings/themes']")
    private WebElement themes;
    @FindBy(xpath = "//a[@href='/messages/inbox/']")
    private WebElement incomming;
    @FindBy(xpath = "//a[@href='/messages/sent/']")
    private WebElement outgoing;
    @FindBy(xpath = "//a[@href='/messages/spam/']")
    private WebElement spamFolder;
    @FindBy(xpath = "//a[@href='/messages/drafts/']")
    private WebElement drafts;
    @FindBy(xpath = "//a[@href='/messages/trash/']")
    private WebElement trash;
    @FindBy(xpath = "//div[@data-cache-key='500000_undefined_false']//div[@data-name='remove']/span")
    private WebElement deleteButton;
    @FindBy(xpath = "//div[@data-name='spam']/span")
    private WebElement reportSpam;
    @FindBy(linkText = "Переместить")
    private WebElement moveMenu;

    private void clickOutgoing(){outgoing.click();}
    private void clickDrafts(){drafts.click();}
    private void clickDeleteButton(){deleteButton.click();}
    private void clickReportSpamButton(){spamFolder.click();}
    private void clickTash(){trash.click();}
    private void clickSpamFolder(){spamFolder.click();}
    private void clickIncomingMail(){incomming.click();}
    private void clickContactsLink(){ contacts.click();}
    private void clickNewMail(){writeMailButton.click();}

    public void selectMailbySubject(String subject)
    {
      try
      {
          webDriver.findElement(By.xpath(String.format("//div[contains(@data-model,'%s')]", subject))).click();
      }
      catch (WebDriverException e1)
      { webDriver.findElement(By.xpath(String.format("//div[contains(text(),'%s')]/../../../div[@class='js-item-checkbox b-datalist__item__cbx']/div", subject))).click();}

    }
    public void navigateToIncomingFolder(){clickIncomingMail();}
    public void navigateToOutgoingFolder(){clickOutgoing();}
    public void navigateToDraftsFolder() { clickDrafts(); }
    public void navigateToTrashFolder() { clickTash(); }
    public void navigateToSpamFolder() { clickSpamFolder(); }
    public void createNewEmail(){clickNewMail();}
    public void checkSendedEmailExist(String subject,String emailTo) throws InterruptedException
    {
        List<WebElement> list = webDriver.findElements(By.xpath(String.format("//div[text()='%s']/../div[text()='"+emailTo+"']",subject)));
        if(list.size()==1)
        {
            Reporter.log("Message was sended");
        }
        else Assert.fail("Message was not sended");
    }
    public void deleteSendedEmail(String subject,String emailTo)
    {
        webDriver.findElement(By.xpath(String.format("//div[text()='%s']/../div[text()='" + emailTo + "']/../../../div[@class='js-item-checkbox b-datalist__item__cbx']",subject))).click();
        try{clickDeleteButton();}
        catch (ElementNotVisibleException e1){webDriver.findElement(By.xpath("//div[@data-cache-key='500000_undefined_false']//div[@data-name='remove']/span")).click();}

    }
    public void deleteSelectedEmail()
    {
        try{clickDeleteButton();}
        catch (ElementNotVisibleException e1){webDriver.findElement(By.xpath("//div[@data-cache-key='0_undefined_false']//div[@data-name='remove']/span")).click();}
        catch (NoSuchElementException e1){webDriver.findElement(By.xpath("//div[@data-cache-key='0_undefined_false']//div[@data-name='remove']/span")).click();}

    }
    public void checkSendedEmailNotExist(String subject,String emailTo) throws InterruptedException {

        try
        {
            shouldNotBeVisible(By.xpath(String.format("//div[text()='%s']/../div[text()='" + emailTo + "']", subject)));
        }
        catch (TimeoutException e1){ Assert.fail("Message was not delete");}
    }
    public void checkThatMessageIsDeleted(String subject) throws InterruptedException {

        List<WebElement> list= webDriver.findElements(By.xpath(String.format("//div[contains(text(),'%S')]/../../../div[@class='js-item-checkbox b-datalist__item__cbx']", subject)));
        if(list.size()==0)
        {
            Reporter.log("MEssage was deleted");
            webDriver.navigate().refresh();
        }
        else
        { Reporter.log("Expected Email was found");
            Assert.fail("Expected message was not found, subject - " + subject);
        }

    }
    public void waitEmailBySubject(String subject,String emailTo) throws InterruptedException {
        for(int i=0; i<50; i++)
        {
            List<WebElement> list= webDriver.findElements(By.xpath(String.format("//div[text()='%s']/../div[text()='" + emailTo + "']", subject)));
            if(list.size()==0){
                Reporter.log("PRESS REFRESH AND WAIT EXPECTED EMAIL");
                webDriver.navigate().refresh();
                sleep(5);
                return;
            }
            else
                Reporter.log("Expected Email was found");
        }
    }
}
