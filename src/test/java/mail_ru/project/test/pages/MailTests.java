package mail_ru.project.test.pages;

import mail_ru.project.test.util.PropertyLoader;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.*;

import javax.mail.MessagingException;
import java.io.IOException;

public class MailTests extends TestBase {

	HomePage homepage;
	MailPage mailPage;
	SendMailPage sendMailPage;

	String subject = PropertyLoader.loadProperty("email.subject")+" "+getDate();
	String emailTo = PropertyLoader.loadProperty("email.to");
	String login = PropertyLoader.loadProperty("login.name");
	String password = PropertyLoader.loadProperty("login.password");

	@BeforeClass
	public void testInit() {

		// Load the page in the browser
		webDriver.get(websiteUrl);
		homepage = PageFactory.initElements(webDriver, HomePage.class);
		mailPage = PageFactory.initElements(webDriver, MailPage.class);
		sendMailPage = PageFactory.initElements(webDriver, SendMailPage.class);

	}

	@Test(description = "Log in and check navigations")
	public void loginAndNavigationInMail() throws InterruptedException {
		homepage.loginInMail(login, password);
		homepage.checkThatLoginIsCompleted();
		Reporter.log("Login is completed|");
		homepage.checkThatCorrectUserLoginIn(login);
		Reporter.log("Check Correct User Log in|");
		mailPage.navigateToOutgoingFolder();
		Reporter.log("Navigate to Outgoing|");
		mailPage.navigateToDraftsFolder();
		Reporter.log("Navigate to Drafts|");
		mailPage.navigateToSpamFolder();
		Reporter.log("Navigate to Spam|");
		mailPage.navigateToTrashFolder();
		Reporter.log("Navigate to Trash|");
		homepage.logOut();
		Reporter.log("Loggout");
	}

	@Test(description = "Create new message and sent it")
	public void sendMailByUI() throws InterruptedException {
		homepage.loginInMail(login, password);
		Reporter.log("Login is completed|");
		mailPage.createNewEmail();
		Reporter.log("Message is created|");
		sendMailPage.typeEmailToAndSubject(emailTo, subject);
		Reporter.log("EmailTo and Subject were filled|");
		sendMailPage.sendEmptyMessage();
		Reporter.log("Message was sended|");
		sendMailPage.checkResultOfSending();
		Reporter.log("Status was checked|");
		mailPage.logOut();
		Reporter.log("Loggout|");
	}

	@Test(description = "Check outgoing message and delete it",dependsOnMethods = "sendMailByUI")
	public void checkSendedMessageAndDelete() throws InterruptedException, IOException, MessagingException {
		homepage.loginInMail(login, password);
		Reporter.log("Login is completed|");
		mailPage.navigateToOutgoingFolder();
		Reporter.log("Navigate to Outgoing|");
		mailPage.checkSendedEmailExist(emailTo, subject);
		Reporter.log("Check that sended message is present in Outgoing|");
		mailPage.deleteSendedEmail(emailTo, subject);
		Reporter.log("Delete sended message from Outgoing|");
		mailPage.checkSendedEmailNotExist(emailTo, subject);
		Reporter.log("Check that sended message is not present in Outgoing|");
		mailPage.logOut();
		Reporter.log("Loggout");
	}

	@Test(description = "Check incoming message and delete it",dependsOnMethods = "sendMailByUI")
	public void checkIncomingMessageAndDelete() throws InterruptedException, IOException, MessagingException {
		homepage.loginInMail(login, password);
		Reporter.log("Login is completed|");
		mailPage.navigateToIncomingFolder();
		Reporter.log("Navigate to Incoming|");
		mailPage.waitEmailBySubject(emailTo, subject);
		Reporter.log("Email waiting is completes|");
		mailPage.selectMailbySubject(subject);
		Reporter.log("Select message by sybject|");
		mailPage.deleteSelectedEmail();
		Reporter.log("Delete selected message|");
		mailPage.checkSendedEmailNotExist(emailTo, subject);
		Reporter.log("Check that message is not present in Incoming|");
		mailPage.logOut();
		Reporter.log("Loggout");
	}

	//Need
	@Test(description = "Send mail by SMTP,Need open 25 port",enabled = false)
	public void checkIncomingMessageSMTPAndDelete() throws InterruptedException, IOException, MessagingException {
		sendMailPage.sendMessageSMTP(emailTo,subject);
		homepage.loginInMail(login, password);
		Reporter.log("Login is completed|");
		mailPage.navigateToIncomingFolder();
		Reporter.log("Navigate to Incoming|");
		mailPage.selectMailbySubject(subject);
		Reporter.log("Select message by sybject|");
		mailPage.deleteSelectedEmail();
		Reporter.log("Delete selected message|");
		mailPage.checkSendedEmailNotExist(emailTo, subject);
		Reporter.log("Check that message is not present in Incoming|");
		mailPage.logOut();
		Reporter.log("Loggout");
	}
}
