package mail_ru.project.test.pages;

import mail_ru.project.test.util.PropertyLoader;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class RegistrationTests extends TestBase {

	HomePage homepage;
	RegistrationPage registrationPage;
	String login = PropertyLoader.loadProperty("login.name");
	String password = PropertyLoader.loadProperty("login.password");
	@BeforeClass
	public void testInit() {
		homepage = PageFactory.initElements(webDriver, HomePage.class);
		registrationPage = PageFactory.initElements(webDriver, RegistrationPage.class);
	}

	@Test
	public void checkRegistrationPage() throws InterruptedException {
		webDriver.get(websiteUrl);
		homepage.navigateToRegistrationPage();
		registrationPage.fillEmailAndPasword(login, password);
		registrationPage.fillBithday("18", "Февраль", "1990");
		registrationPage.finishRegistration();
		registrationPage.checkErrorsValidationIsExist();
	}

	@Test
	public void checkREgistrationNotCompleted() throws InterruptedException {
		webDriver.get(websiteUrl);
		homepage.navigateToRegistrationPage();
		registrationPage.fillNameAndSecondNAme("NNNN", "BBBB");
		registrationPage.fillEmailAndPasword("areutasg44",password);
		registrationPage.fillBithday("18","Февраль","1990");
		registrationPage.finishRegistration();
	}
}
