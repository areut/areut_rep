package mail_ru.project.test.pages;


import mail_ru.project.test.util.PropertyLoader;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTests extends TestBase {
	String login = PropertyLoader.loadProperty("login.name");
	String password = PropertyLoader.loadProperty("login.password");
	HomePage homepage;
	@BeforeClass
	public void testInit() {
		webDriver.get(websiteUrl);
		Reporter.log("Open Site PAge");
		homepage = PageFactory.initElements(webDriver, HomePage.class);
	}


	@Test(description = "check sucsefully login in the system",priority = 0)
	public void loginSuccsefully() throws InterruptedException {
		Reporter.log("Site is opened");
		homepage.loginInMail(login, password);
		Reporter.log("Loggin at mail.ru");
		homepage.checkThatCorrectUserLoginIn(login);
		Reporter.log("Checked that user is correct");
		homepage.logOut();
		Reporter.log("Logout is completed");
	}

	@Test(description = "check negative login in the system",priority = 1)
	public void loginNegative() throws InterruptedException {
		webDriver.get(websiteUrl);
		Reporter.log("Site is opened");
		homepage.loginInMail(login, "Aksfsfak5335");
		Reporter.log("Type Inccorect login and password");
		homepage.checkErrorDuringLogin();
		Reporter.log("Check validation");
		//check validation
	}
}
